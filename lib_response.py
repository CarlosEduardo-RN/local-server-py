
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/api/mandar-foto", methods=['POST'])
def receivedPic():
    body = request.json
    print(body['foto'])

    return jsonify("{result: 'recebido'}")

@app.route("/api/mandar-placa")
def receivedPlate():
    return jsonify("{nome: 'Thiago'}")


def init():
    app.run()