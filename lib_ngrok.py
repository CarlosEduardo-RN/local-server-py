from pyngrok import ngrok
import lib_request


def generateNgrok():
    http_tunnel = ngrok.connect(5000)
    stringVariable = str(http_tunnel)
    stringCortada = stringVariable.split(" ")
    lib_request.conectaApi(stringCortada[1].replace('"', ""))
